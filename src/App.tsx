import React from "react";

import "./App.scss";
import Navbar from "./components/homePage/navbar/Navbar";
import Footer from "./components/homePage/footer/Footer";
import { BrowserRouter as Router, Route, Routes } from "react-router-dom";
import HomePage from "./pages/HomePage";
import Menu from "./pages/Menu";
import CookDetails from "./pages/CookDetails";
import CookProvider from "./contex/CookProvider";
import Cooks from "./pages/Cooks";
import How from "./pages/How";
import ScrollToTop from "./components/scrollToTop/ScrollToTop";
import ErrorPage from "./pages/ErrorPage";



function App() {
 
  return (
    <div>
      <CookProvider>
        <Router>
          <ScrollToTop >
          <Navbar />
          <Routes>
            <Route path="/" element={<HomePage />}></Route>
            <Route path="/menu" element={<Menu />}></Route>
            <Route path="/cookDetails/:id" element={<CookDetails />}></Route>
            <Route path="/cooks" element={<Cooks />}></Route>
            <Route path="/how" element={<How />}></Route>
            <Route path="*" element={<ErrorPage />} />
          </Routes>
          <Footer />
          </ScrollToTop>
        </Router>
      </CookProvider>
    </div>
  );
}

export default App;
