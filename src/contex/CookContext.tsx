import { createContext, useContext } from "react"
import { CookType } from "./CookProvider"




export const CookContext = createContext({} as CookType)

export const useCookContext = () => useContext(CookContext)