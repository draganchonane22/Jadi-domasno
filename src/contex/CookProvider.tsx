import React, { useEffect, useState } from "react";
import { CookContext } from "./CookContext";
import { useFetch } from "../hooks/useFetch";
import { HomePageDescription } from "../Types";
import { FeedBackData } from "../components/details/feedback/FeedBack";
import { Alergeni } from "../components/menu/searchBar/SearchBar";
import { MenuData } from "../components/menu/menuCards/MenuCardAll";
import { Users } from "../components/homePage/userCards/UserCards";
import { Cuisines } from "../components/cooks/SearchCooks";
import { CardImages } from "../components/how/cardImage/CardImage";
import { BecomeCooks } from "../components/how/howBecome/HowBecome";
import { CookTypesD } from "../components/details/detailsHeader/DetailsHeader";
import { CardImageSm } from "../components/how/cardImageSmall/CardImageSmall";

type Props = {
  children: React.ReactNode;
};

export type CookType = {
  cooksData: CookTypesD[];
  descData: HomePageDescription[];
  feedBackData: FeedBackData[];
  alergeni: Alergeni[];
  mondayData: MenuData[];
  userData: Users[];
  stateAlergeni: string;
  cuisines: Cuisines[];
  stateCuisines: string;
  value: number | number[] | undefined;
  availableFood: string;
  ratingFilter: string;
  deliveryFilter: string;
  ratingCuisines: string;
  howState: string;
  show: number;
  howCookData: CardImages[];
  howCookDataG: CardImages[];
  howImageS: CardImageSm[];
  howStickerC: BecomeCooks[];
  howStickerG: BecomeCooks[];
  showItems: (id: MenuData[]) => void;
  showItemss: (id: CookTypesD[]) => void;
  handleDeliveryFilter: (id: string) => void;
  howRender: (id: string) => void;
  handleRatingCuisines: (id: string) => void;
  handleRatingFilter: (id: string) => void;
  handleAvailableFood: (id: string) => void;
  handleButton: (id: string) => void;
  handleCuisines: (id: string) => void;
  clearFilter: (id: string) => void;
  handleChange: (event: Event, newValue: number | number[]) => void;
  initModal: (id: boolean, foodName: string, cookId: number) => void;
  handleActive: (id: string) => void;
  handleAddFavorite: (res: FeedBackData) => void;
  handleRemoveFavorite: (res: FeedBackData) => void;
  isShow: boolean; 
  foodName: string;
  cookId: number;
  active: string;
  favorites: FeedBackData[];
};

export default function CookProvider({ children }: Props) {
  const [favorites, setFavorites] = useState<FeedBackData[]>([]);
  useEffect(() => {
    const localFavs: FeedBackData[] = JSON.parse(
      localStorage.getItem("currentFavorites") || "[]"
    );
    setFavorites(localFavs);
  }, []);
 
  const mondayMenuData = useFetch<MenuData>("http://localhost:5001/mondayMenu");
  const [cooksData, setCooksData] = useState<CookTypesD[]>([]);
  const [descData, setDescData] = useState<HomePageDescription[]>([]);
  const [feedBackData, setFeedBackData] = useState<FeedBackData[]>([]);
  const [alergeni, setAlergeni] = useState<Alergeni[]>([]);
  const [mondayData, setMondayData] = useState<MenuData[]>(mondayMenuData.data);
  const [userData, setUserData] = useState<Users[]>([]);
  const [stateAlergeni, setStateAlergeni] = useState("");
  const [value, setValue] = useState<number | number[]>();
  const [availableFood, setAvailableFood] = useState("");
  const [ratingFilter, setRatingFilter] = useState("");
  const [deliveryFilter, setDeliveryFilter] = useState("");
  const [stateCuisines, setStateCuisines] = useState("");
  const [cuisines, setCuisines] = useState<Cuisines[]>([]);
  const [ratingCuisines, setRatingCuisines] = useState("");
  const [show, setShow] = useState(6);
  const [howState, setHowState] = useState("cook");
  const [howCookData, setHowCookData] = useState<CardImages[]>([]);
  const [howCookDataG, setHowCookDataG] = useState<CardImages[]>([]);
  const [howImageS, setHowImageS] = useState<CardImageSm[]>([]);
  const [howStickerC, setHowStickerC] = useState<BecomeCooks[]>([]);
  const [howStickerG, setHowStickerG] = useState<BecomeCooks[]>([]);
  const [isShow, setIsShow] = useState(false);
  const [foodName, setFoodName] = useState("");
  const [cookId, setCookId] = useState(0);
  const [active, setActive] = useState("/");
 

  const dataCook = useFetch<CookTypesD>("http://localhost:5001/homePageCook");

  const descriptionData = useFetch<HomePageDescription>(
    "http://localhost:5001/homePageDescription"
  );
  const feedBackDetailsData = useFetch<FeedBackData>(
    "http://localhost:5001/detailsFeedBack"
  );
  const btnAlergeniData = useFetch<Alergeni>(
    "http://localhost:5001/btnAlergeni"
  );
  const btnCuisinesData = useFetch<Cuisines>(
    "http://localhost:5001/btnCuisines"
  );
  const usersData = useFetch<Users>("http://localhost:5001/homePageUsers");
  const howImageData = useFetch<CardImages>(
    "http://localhost:5001/howHeadCardsCooks"
  );
  const howImageDataG = useFetch<CardImages>(
    "http://localhost:5001/howHeadCardsGurman"
  );
  const howImageSmall = useFetch<CardImageSm>(
    "http://localhost:5001/howCooksCards"
  );
  const howStickerCook = useFetch<BecomeCooks>(
    "http://localhost:5001/howStickerCooks"
  );
  const howStickerGurman = useFetch<BecomeCooks>(
    "http://localhost:5001/howStickerGurman"
  );

  

  useEffect(() => {
    setCooksData(dataCook.data);
  }, [dataCook.data]);
  useEffect(() => {
    setDescData(descriptionData.data);
  }, [descriptionData.data]);
  useEffect(() => {
    setFeedBackData(feedBackDetailsData.data);
  }, [feedBackDetailsData.data]);
  useEffect(() => {
    setAlergeni(btnAlergeniData.data);
  }, [btnAlergeniData.data]);
  useEffect(() => {
    setCuisines(btnCuisinesData.data);
  }, [btnCuisinesData.data]);
  useEffect(() => {
    setMondayData(mondayMenuData.data);
  }, [mondayMenuData.data]);
  useEffect(() => {
    setUserData(usersData.data);
  }, [usersData]);
  useEffect(() => {
    setHowCookData(howImageData.data);
  }, [howImageData.data]);
  useEffect(() => {
    setHowCookDataG(howImageDataG.data);
  }, [howImageDataG.data]);
  useEffect(() => {
    setHowImageS(howImageSmall.data);
  }, [howImageSmall.data]);
  useEffect(() => {
    setHowStickerC(howStickerCook.data);
  }, [howStickerCook.data]);
  useEffect(() => {
    setHowStickerG(howStickerGurman.data);
  }, [howStickerGurman.data]);
  const handleButton = (id: string) => {
    setShow(6);
    setStateAlergeni(id);
  };
  const handleCuisines = (id: string) => {
    setShow(6);
    setStateCuisines(id);
  };
  const handleChange = (event: Event, newValue: number | number[]) => {
    setShow(6);
    setValue(newValue);
  };
  const handleAvailableFood = (id: string) => {
    setShow(6);
    setAvailableFood(id);
  };
  const handleRatingFilter = (id: string) => {
    setShow(6);
    setRatingFilter(id);
  };
  const handleRatingCuisines = (id: string) => {
    setShow(6);
    setRatingCuisines(id);
  };
  const handleDeliveryFilter = (id: string) => {
    setShow(6);
    setDeliveryFilter(id);
  };
  const howRender = (id: string) => {
    setHowState(id);
  };
  const showItems = (filter: MenuData[]) => {
    const showItem = filter.length + 3;
    setShow(showItem);
  };
  const showItemss = (filter: CookTypesD[]) => {
    const showItem = filter.length + 3;
    setShow(showItem);
  };
  const initModal = (id: boolean, foodName: string, cookId: number) => {
    setIsShow(id);
    setFoodName(foodName);
    setCookId(cookId);
  };
  const handleActive = (id: string) => {
    setActive(id);
  };

  const handleAddFavorite = (res: FeedBackData) => {
    setFavorites([...favorites, res]);
  };
  
  const handleRemoveFavorite = (res: FeedBackData) => {
    setFavorites(favorites.filter(favRes => favRes.id !== res.id));
  };

  useEffect(() => {
    localStorage.setItem("currentFavorites", JSON.stringify(favorites));
  }, [favorites]);
 


  const clearFilter = (id: string) => {
    id === "alergeni" && setStateAlergeni("");
    id === "available" && setAvailableFood("");
    id === "rating" && setRatingFilter("");
    id === "delivery" && setDeliveryFilter("");
    id === "cuisines" && setStateCuisines("");
    id === "ratingCuisines" && setRatingCuisines("");
  };

  const ctxValue: CookType = {
    cooksData,
    descData,
    feedBackData,
    alergeni,
    mondayData,
    userData,
    stateAlergeni,
    handleButton,
    clearFilter,
    handleChange,
    handleAvailableFood,
    availableFood,
    ratingFilter,
    handleRatingFilter,
    handleDeliveryFilter,
    handleCuisines,
    handleRatingCuisines,
    showItems,
    showItemss,
    howRender,
    deliveryFilter,
    stateCuisines,
    cuisines,
    ratingCuisines,
    value,
    howState,
    howCookData,
    howCookDataG,
    howImageS,
    howStickerC,
    howStickerG,
    show,
    isShow,
    foodName,
    cookId,
    initModal,
    handleActive,
    active,
    favorites,
    handleAddFavorite,
    handleRemoveFavorite
    
  };

  return (
    <CookContext.Provider value={ctxValue}>{children}</CookContext.Provider>
  );
}
