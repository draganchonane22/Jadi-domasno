import React from "react";
import "./HowLogo.scss";
import { useCookContext } from "../../../contex/CookContext";

export default function HowLogo() {
  const { howRender, howState } = useCookContext();
  return (
    <div className="dek">
      <img className="castom-position7" src="./images/img4.png" alt="img" />
      <img className="castom-position8" src="./images/img4.png" alt="img" />
      <div className="logo">
        <img className="w-100" src="/images/logo.png" alt="img" />
      </div>
      <div className="mb-3">
        <div className="row wrapper1 justify-content-center">
          <div className="col-2 my-auto de">
            <hr />
          </div>
          <div className="col-4 text-center">
            <p className="font-big">Како Функционира?</p>
          </div>
          <div className="col-2 my-auto de">
            <hr />
          </div>
        </div>
      </div>
      <div className="btn-how my-5">
        <div
          className={`btn-left ${howState === "cook" ? "actives" : ""}`}
          onClick={() => howRender("cook")}
        >
          Готвачи
        </div>
        <div
          className={`btn-right ${howState === "gurman" ? "actives" : ""}`}
          onClick={() => howRender("gurman")}
        >
          Гурмани
        </div>
      </div>
      <p className="w-75 mx-auto my-5">
        Lorem ipsum dolor sit amet consectetur adipisicing elit. Fugiat
        excepturi maxime hic libero magni voluptate eveniet <br /> qui sed odit
        alias ratione est at quibusdam, esse molestiae aliquam impedit ullam.
        Doloribus. Lorem, ipsum dolor sit amet consectetur adipisicing elit.
        Sed, ipsum atque eum voluptatem vero illum, delectus impedit, incidunt
        ullam ratione sapiente quidem facilis laboriosam quia rerum ab in! Quod,
        veniam? Lorem ipsum dolor sit amet, consectetur adipisicing elit.
        Perspiciatis numquam porro facilis cupiditate autem quas, eum eaque,
        esse error minus laudantium quisquam animi doloremque iure corporis?
        Voluptates, explicabo? Earum, eos?
      </p>
    </div>
  );
}
