import React from "react";
import "./CardImageSmall.scss";

export interface CardImageSm {
  id: number;
  imageUrl: number;
  title: string;
}

export default function CardImageSmall({imageUrl,title}: CardImageSm) {
  return (
    <div className="col-6 d-flex justify-content-center align-items-center my-5">
      <div className="main-div">
        <div className="wrapper-img">
        <img className="w-100" src={`./images/howSmall${imageUrl}.jpg`} alt="img" />
        </div>
        <span>{title}</span>
      </div>
    </div>
  );
}
