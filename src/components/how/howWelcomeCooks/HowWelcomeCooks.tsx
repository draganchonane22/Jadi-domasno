import React from "react";
import "./HowWelcomeCooks.scss"



export default function HowWelcome() {
  return (
    <div>
      <div className="row wrapper1 mb-5">
        <div className="col-5 my-auto de">
          <hr />
        </div>
        <div className="col-2 text-center">
          <img src="/images/domat1.png" alt="img" />
        </div>
        <div className="col-5 my-auto de">
          <hr />
        </div>
      </div>
      <div className="w-50 mx-auto text-center dek">
        <h4 className="mb-5">Ви благодариме и добредојдовте!</h4>
        <p className="mb-5">
          Ви посакуваме успешна работа,многу готвење и споделување убави моменти
          со храната!
        </p>
        <button className="mb-5">регистрирај се</button>
      </div>
      <div className="row wrapper1 mb-5">
        <div className="col-5 my-auto de">
          <hr className="bg-dark"/>
        </div>
        <div className="col-2 text-center">
          <img className="w-100" src="/images/list1.png" alt="img" />
        </div>
        <div className="col-5 my-auto de">
          <hr className="bg-dark"/>
        </div>
      </div>
    </div>
  );
}
