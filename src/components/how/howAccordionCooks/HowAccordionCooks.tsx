import Accordion from "@mui/material/Accordion";
import AccordionDetails from "@mui/material/AccordionDetails";
import AccordionSummary from "@mui/material/AccordionSummary";
import Typography from "@mui/material/Typography";
import ExpandMoreIcon from "@mui/icons-material/ExpandMore";
import React, { useState } from "react";
import "./HowAccordionCooks.scss";

export default function HowCarousel() {
  const [expanded, setExpanded] = useState<string | false>(false);

  const handleChange =
    (panel: string) => (event: React.SyntheticEvent, isExpanded: boolean) => {
      setExpanded(isExpanded ? panel : false);
    };
  return (
    <div>
      <div className="my-5">
        <div className="row wrapper1 justify-content-center">
          <div className="col-6 text-center">
            <p className="font-big">Најчесто поставувани прашања</p>
          </div>
        </div>
      </div>

      <div className="w-75 mx-auto my-5">
        <Accordion
          expanded={expanded === "panel1"}
          onChange={handleChange("panel1")}
          className="bg__accordion"
        >
          <AccordionSummary
            expandIcon={<ExpandMoreIcon />}
            aria-controls="panel1bh-content"
            id="panel1bh-header"
          >
            <Typography sx={{ color: "black", fontWeight: "700" }}>
              Како да се регистрирам?
            </Typography>
          </AccordionSummary>
          <AccordionDetails>
            <Typography sx={{ color: "black", fontWeight: "600" }}>
              Nulla facilisi. Phasellus sollicitudin nulla et quam mattis
              feugiat. Aliquam eget maximus est, id dignissim quam.
            </Typography>
          </AccordionDetails>
        </Accordion>
        <Accordion
          expanded={expanded === "panel2"}
          onChange={handleChange("panel2")}
          className="bg__accordion"
        >
          <AccordionSummary
            expandIcon={<ExpandMoreIcon />}
            aria-controls="panel2bh-content"
            id="panel2bh-header"
          >
            <Typography sx={{ color: "black", fontWeight: "700" }}>
              Како да променам коментар?
            </Typography>
          </AccordionSummary>
          <AccordionDetails>
            <Typography sx={{ color: "black", fontWeight: "600" }}>
              Donec placerat, lectus sed mattis semper, neque lectus feugiat
              lectus, varius pulvinar diam eros in elit. Pellentesque convallis
              laoreet laoreet.
            </Typography>
          </AccordionDetails>
        </Accordion>
        <Accordion
          expanded={expanded === "panel3"}
          onChange={handleChange("panel3")}
          className="bg__accordion"
        >
          <AccordionSummary
            expandIcon={<ExpandMoreIcon />}
            aria-controls="panel3bh-content"
            id="panel3bh-header"
          >
            <Typography sx={{ color: "black", fontWeight: "700" }}>
              Како да променам веќе објавена слика на рецепт?
            </Typography>
          </AccordionSummary>
          <AccordionDetails>
            <Typography sx={{ color: "black", fontWeight: "600" }}>
              Nunc vitae orci ultricies, auctor nunc in, volutpat nisl. Integer
              sit amet egestas eros, vitae egestas augue. Duis vel est augue.
            </Typography>
          </AccordionDetails>
        </Accordion>
        <Accordion
          expanded={expanded === "panel4"}
          onChange={handleChange("panel4")}
          className="bg__accordion"
        >
          <AccordionSummary
            expandIcon={<ExpandMoreIcon />}
            aria-controls="panel4bh-content"
            id="panel4bh-header"
          >
            <Typography sx={{ color: "black", fontWeight: "700" }}>
              Како да променам веќе објавена слика на рецепт?
            </Typography>
          </AccordionSummary>
          <AccordionDetails>
            <Typography sx={{ color: "black", fontWeight: "600" }}>
              Nunc vitae orci ultricies, auctor nunc in, volutpat nisl. Integer
              sit amet egestas eros, vitae egestas augue. Duis vel est augue.
            </Typography>
          </AccordionDetails>
        </Accordion>
      </div>
    </div>
  );
}
