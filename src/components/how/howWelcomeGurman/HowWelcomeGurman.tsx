import React from "react";
import "./HowWelcomeGurman.scss";

export default function HowWelcomeGurman() {
  return (
    <div>
      <div className="row wrapper1 mb-5">
        <div className="col-5 my-auto de">
          <hr />
        </div>
        <div className="col-2 text-center">
          <img src="/images/domat1.png" alt="img" />
        </div>
        <div className="col-5 my-auto de">
          <hr />
        </div>
      </div>
      <div className="w-50 mx-auto text-center dek">
        <h4 className="mb-5">Ви благодариме и добредојдовте!</h4>
        <p className="mb-5">
          Ви посакуваме уживање во храната од нашите готвачи и споделување убави
          моменти во соседството!
        </p>
        <button className="mb-5">регистрирај се</button>
      </div>
      <div className="row wrapper1 mb-5">
        <div className="col-5 my-auto de">
          <hr className="bg-dark" />
        </div>
        <div className="col-2 text-center">
          <img className="w-100" src="/images/list1.png" alt="img" />
        </div>
        <div className="col-5 my-auto de">
          <hr className="bg-dark" />
        </div>
      </div>
    </div>
  );
}
