import React from "react";
import "./HowGurman.scss";
import CardImage from "../cardImage/CardImage";
import HowBecome from "../howBecome/HowBecome";
import HomePageCook from "../../homePage/homePageCook/HomePageCook";
import HowWelcomeGurman from "../howWelcomeGurman/HowWelcomeGurman";
import HowAccordion from "../howAccordionCooks/HowAccordionCooks";
import { useCookContext } from "../../../contex/CookContext";



export default function HowGurman() {
  const { howCookDataG, howStickerG } = useCookContext();
  return (
    <div>
      <div className="my-5">
        <div className="row wrapper1 justify-content-center">
          <div className="col-3 my-auto de">
            <hr />
          </div>
          <div className="col-6 text-center">
            <p className="font-big">Нашата мисија и визија</p>
          </div>
          <div className="col-3 my-auto de">
            <hr />
          </div>
        </div>
      </div>
      <div className="row w-75 mx-auto">
        {howCookDataG.map((item) => (
          <CardImage key={item.id} {...item}></CardImage>
        ))}
      </div>
      <div className="my-5">
        <div className="row wrapper1 justify-content-center">
          <div className="col-2 my-auto de">
            <hr />
          </div>
          <div className="col-8 text-center">
            <p className="font-big">Како да станете дел од јади домашно</p>
          </div>
          <div className="col-2 my-auto de">
            <hr />
          </div>
        </div>
      </div>
      <div>
      {howStickerG.map((item) => (
          <HowBecome key={item.id} {...item}></HowBecome>
        ))}
      </div>
      <div>
        <HomePageCook />
      </div>
      <div>
        <HowWelcomeGurman />
      </div>
      <div>
        <HowAccordion />
      </div>
    </div>
  );
}
