import React from "react";
import "./HowCooks.scss";
import CardImage from "../cardImage/CardImage";
import CardImageSmall from "../cardImageSmall/CardImageSmall";
import HowBecome from "../howBecome/HowBecome";
import HomePageUsers from "../../homePage/homePageUsers/HomePageUsers";
import HowWelcomeCooks from "../howWelcomeCooks/HowWelcomeCooks";
import HowAccordionCooks from "../howAccordionCooks/HowAccordionCooks";
import { useCookContext } from "../../../contex/CookContext";

export default function HowCooks() {
  const { howCookData, howImageS, howStickerC } = useCookContext();
  return (
    <div>
      <div className="my-5">
        <div className="row wrapper1 justify-content-center">
          <div className="col-2 my-auto de">
            <hr />
          </div>
          <div className="col-8 text-center">
            <p className="font-big">Зошто да станете готвач на Јади домашно?</p>
          </div>
          <div className="col-2 my-auto de">
            <hr />
          </div>
        </div>
      </div>
      <div className="row w-75 mx-auto">
        {howCookData.map((item) => (
          <CardImage key={item.id} {...item}></CardImage>
        ))}
      </div>
      <div>
        <div className="my-5">
          <div className="row wrapper1 justify-content-center">
            <div className="col-2 my-auto de">
              <hr />
            </div>
            <div className="col-8 text-center">
              <p className="font-big">
                Кои критериуми треба да ги исполнува еден готвач?
              </p>
            </div>
            <div className="col-2 my-auto de">
              <hr />
            </div>
          </div>
        </div>
      </div>
      <div className="row w-75 mx-auto">
        {howImageS.map((item) => (
          <CardImageSmall key={item.id} {...item}></CardImageSmall>
        ))}
      </div>
      <div>
        <div className="my-5">
          <div className="row wrapper1 justify-content-center">
            <div className="col-2 my-auto de">
              <hr />
            </div>
            <div className="col-8 text-center">
              <p className="font-big">
                Како да станете готвач на јади домашно?
              </p>
            </div>
            <div className="col-2 my-auto de">
              <hr />
            </div>
          </div>
        </div>
      </div>
      <div>
        {howStickerC.map((item) => (
          <HowBecome key={item.id} {...item}></HowBecome>
        ))}
      </div>
      <div>
        <div className="my-5">
          <div className="row wrapper1 justify-content-center">
            <div className="col-2 my-auto de">
              <hr />
            </div>
            <div className="col-8 text-center">
              <p className="font-big">Што рекоа нашите задоволни гурмани?</p>
            </div>
            <div className="col-2 my-auto de">
              <hr />
            </div>
          </div>
        </div>
      </div>
      <div>
        <HomePageUsers />
      </div>
      <div>
        <HowWelcomeCooks />
      </div>
      <div>
        <HowAccordionCooks />
      </div>
    </div>
  );
}
