import React from "react";
import "./HowBecome.scss";

export interface BecomeCooks {
  id: number;
  imageUrl: number;
  title: string;
  description: string;
}

export default function HowBecome({
  imageUrl,
  title,
  description,
}: BecomeCooks) {
  return (
    <div className="d-flex col-6 mx-auto my-5 about-sticker">
      <div>
        <img src={`./images/sticker${imageUrl}.png`} alt="img" />
      </div>
      <div className="ml-5">
        <h5>{title}</h5>
        <p>{description}</p>
      </div>
    </div>
  );
}
