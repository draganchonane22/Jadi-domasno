import React from "react";
import "./CardImage.scss";

export interface CardImages {
  id: number;
  imageUrl: number;
  title: string;
  description: string;
}

export default function CardImage({imageUrl,title,description}: CardImages) {
  return (
    <div className="col-4 p-3 about-cookCard">
      <img
        className="mb-3 about__img"
        src={`./images/how${imageUrl}.jpg`}
        alt="img"
      />
      <h5 className="mb-3 text-center">{title}</h5>
      <p className="text-center">
       {description}
      </p>
    </div>
  );
}
