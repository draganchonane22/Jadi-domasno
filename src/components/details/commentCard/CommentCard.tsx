import React from "react";
import "./CommentCard.scss";
import { Users } from "../../homePage/userCards/UserCards";



export default function CommentCard({
  imageUrl,
  date,
  rating,
  description,
  name,
}: Users) {
  return (
    <div className="col-12 mb-3">
      <div className="row">
        <div className="col-3">
          <img
            className="about__img-comment"
            src={`../images/user${imageUrl}.png`}
            alt="img"
          />
        </div>
        <div className="col-9 about__underline">
          <span className="mr-3">{date}</span>
          <span className="mr-3">{name}</span>
          <span>
            {Array.from(Array(rating).keys()).map((_, i) => (
              <i key={`star-${i}`} className="fa fa-star"></i>
            ))}
          </span>
          <p className="mb-3">{description} </p>
        </div>
      </div>
    </div>
  );
}
