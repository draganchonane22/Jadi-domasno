import React, { useState } from "react";
import "./FoodCard.scss";
import { Rating } from "@mui/material";
import { CiDeliveryTruck } from "react-icons/ci";
import { TbTruckOff } from "react-icons/tb";
import { AiOutlineClockCircle } from "react-icons/ai";
import { MenuData } from "../../menu/menuCards/MenuCardAll";
import { useCookContext } from "../../../contex/CookContext";

export default function FoodCard({
  imageUrl,
  cookId,
  foodName,
  price,
  rating,
  delivery,
  time,
  description,
}: MenuData) {
  const { initModal } = useCookContext();
  const [show, setShow] = useState(false);
  return (
    <div className="col-4">
      <div className="card about__card px-0 my-4">
        <div className="position-relativ">
          <img
            src={`/images/hrana${imageUrl}.jpg`}
            className="card-img-top"
            alt="img"
          />
        </div>

        <i
          onClick={() => setShow(!show)}
          className={`about__icons about-position2 ${
            show ? "fas" : "far"
          } fa-heart fa-3x`}
        />
        {delivery === "yes" ? (
          <CiDeliveryTruck className="about__icons about-position1" />
        ) : (
          <TbTruckOff className="about__icons about-position1" />
        )}

        <div className="card-body">
          <div className="d-flex justify-content-between">
            <h5 className="card-title">{foodName}</h5>
            <span className="sp">{price}den</span>
          </div>
          <Rating name="read-only" value={rating} readOnly />
          <p className="card-text mb-2">{description}</p>
          <span>
            <AiOutlineClockCircle className="about__icons" />
          </span>
          <span>{time}</span>
          <div className="pt-3">
            <button
              className="btn btn-primary"
              onClick={() => initModal(true, foodName, cookId)}
            >
              dodaj vo kosnicka
            </button>
          </div>
        </div>
      </div>
    </div>
  );
}
