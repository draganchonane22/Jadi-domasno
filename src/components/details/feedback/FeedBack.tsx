import React from "react";
import "./FeedBack.scss";
import { useCookContext } from "../../../contex/CookContext";

export interface FeedBackData {
  id: number;
  cookId: number;
  name: string;
  feedBack: string;
}

interface Props {
  item: FeedBackData;
}

export default function FeedBack({ item }: Props) {
  const { name, feedBack } = item;
  const { favorites, handleAddFavorite, handleRemoveFavorite } =
    useCookContext();
  const isFavorite = favorites.find((favRes) => favRes.id === item.id);
  return (
    <div className="col-4 about-card-radius">
      <div className="card bg-white my-4 px-0 about__card">
        <div className="card-body position-relativ my-5">
          <h5>{name}</h5>
          <p>{feedBack}</p>
          <i
            onClick={() => {
              isFavorite ? handleRemoveFavorite(item) : handleAddFavorite(item);
            }}
            className={`about__icons about-position1 ${
              isFavorite ? "fas" : "far"
            } fa-heart fa-3x`}
          />
        </div>
      </div>
    </div>
  );
}
