import React from "react";
import "./GetLocation.scss"
import Box from "@mui/material/Box";
import Zoom from "@mui/material/Zoom";

type Props = {
  show: boolean;
};

export default function GetLocation({ show }: Props) {
  return (
    <div>
      <Box sx={{ height: 180 }}>
        <Box sx={{ display: "flex" }}>
          <Zoom in={show}>
            <iframe
              width="300"
              height="170"
              scrolling="no"
              id="gmap_canvas"
              src="https://maps.google.com/maps?width=520&amp;height=400&amp;hl=en&amp;q=Leninova%20%20Skopje+()&amp;t=&amp;z=13&amp;ie=UTF8&amp;iwloc=B&amp;output=embed"
            ></iframe>
          </Zoom>
        </Box>
      </Box>
    </div>
  );
}
