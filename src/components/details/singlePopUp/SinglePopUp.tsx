import React, { useState } from "react";
import "./SinglePopUp.scss";
import { Modal, Button } from "react-bootstrap";
import { GiChiliPepper } from "react-icons/gi";
import { useCookContext } from "../../../contex/CookContext";
import { MenuData } from "../../menu/menuCards/MenuCardAll";
import { AiFillStar } from "react-icons/ai";
import { DesktopDatePicker } from "@mui/x-date-pickers/DesktopDatePicker";
import dayjs, { Dayjs } from "dayjs";
import TextField from "@mui/material/TextField";
import { LocalizationProvider } from "@mui/x-date-pickers/LocalizationProvider";
import { AdapterDayjs } from "@mui/x-date-pickers/AdapterDayjs";
import { TimePicker } from "@mui/x-date-pickers/TimePicker";
import CommentCard from "../commentCard/CommentCard";

type Props = {
  cookPopUp: MenuData;
  resultStars: number
};

export default function SinglePopUp({ cookPopUp, resultStars }: Props) {
  const {
    imageUrl,
    cookImageUrl,
    cookName,
    foodName,
    location,
    availablePortion,
    description,
    ingredients,
    nutrition,
    alergies,
  } = cookPopUp;

  const { isShow, initModal, userData } = useCookContext();
  const [show, setShow] = useState(2);
  const [value, setValue] = useState<Dayjs | null>(
    dayjs("2023-02-12T21:11:54")
  );

  const handleChange = (newValue: Dayjs | null) => {
    setValue(newValue);
  };
  const filter = userData.slice(0, show);

  return (
    <div>
      <Modal
        show={isShow}
        scrollable={true}
        onHide={() => initModal(false, "", 0)}
      >
        <Modal.Body className="modal-main">
          <div>
            <img
              className="w-100 mb-3"
              src={`../images/hrana${imageUrl}.jpg`}
              alt="img"
            />
            <div className="row">
              <div className="col-7 mb-3">
                <h4>{foodName}</h4>
                <p className="mb-2">По рецептот на баба</p>
                <img src="../images/Layer3.png" alt="img" />
                <span className="mx-3">Традиционално</span>
                <span>
                  <GiChiliPepper className="text-danger" />
                </span>
                <span>
                  <GiChiliPepper className="text-danger" />
                </span>
                <span>
                  <GiChiliPepper />
                </span>
                <span className="ml-1">луто</span>
              </div>
              <div className="col-5">
                <button className="about-button">Прати порака</button>
              </div>
            </div>
            <div className="d-flex">
              <div className="main-div">
                <img
                  src={`../images/user${cookImageUrl}.png`}
                  className="card-img-top img-main-card"
                  alt="img"
                />
              </div>
              <div className="ml-5">
                <h5>{cookName}</h5>
                <small>Специјализирана по традиционални оброци</small>
                <br />
                <small className="">078 987-765</small>
                <div className="mt-3">
                  <span className="about-span1">
                    <AiFillStar className="about__icons" />{""}{resultStars.toFixed(1)}
                  </span>
                  <span></span>
                </div>
              </div>
            </div>
            <hr />
            <div className="d-flex justify-content-between">
              <h6>Одбери датум и време на нарачка</h6>
              <div>
                <LocalizationProvider dateAdapter={AdapterDayjs}>
                  <div className="mb-3">
                    <DesktopDatePicker
                      label="Date desktop"
                      inputFormat="MM/DD/YYYY"
                      value={value}
                      onChange={handleChange}
                      renderInput={(params) => <TextField {...params}/>}
                    />
                  </div>
                  <div>
                    <TimePicker
                      label="Time"
                      value={value}
                      onChange={handleChange}
                      renderInput={(params) => <TextField {...params}/>}
                    />
                  </div>
                </LocalizationProvider>
              </div>
            </div>
            <hr />
            <div className="d-flex justify-content-between">
              <div>
                <h6 className="mb-3">Одбери количина</h6>
                <small>достапни {availablePortion} порции</small>
              </div>
              <div></div>
            </div>
            <hr />
            <div>
              <h6 className="mb-3">Опис на јадењето</h6>
              <p>{description}</p>
            </div>
            <hr />
            <div>
              <h6 className="mb-3">Главни состојки</h6>
              <p>{ingredients}</p>
            </div>
            <hr />
            <div>
              <h6 className="mb-3">Нутритивни вредности</h6>
              <p>{nutrition}</p>
            </div>
            <hr />
            <div>
              <h6 className="mb-3">Можни алергиски појави</h6>
              <p>{alergies}</p>
            </div>
            <hr />
            <div className="d-flex justify-content-between">
              <div>
                <p>Локација за преземање на нарачка</p>
                <small>{location}m оддалечено од тебе</small>
              </div>
              <div>
                <iframe
                  width="300"
                  height="200"
                  scrolling="no"
                  id="gmap_canvas"
                  src="https://maps.google.com/maps?width=520&amp;height=400&amp;hl=en&amp;q=Leninova%20%20Skopje+()&amp;t=&amp;z=13&amp;ie=UTF8&amp;iwloc=B&amp;output=embed"
                ></iframe>
              </div>
            </div>
            <hr />
            <div>
              <h6 className="mb-3">Што кажаа другите за храната</h6>
              <div className="row">
                {filter.map((item) => (
                  <CommentCard key={item.id} {...item}></CommentCard>
                ))}
                {filter.length < userData.length && userData.length > 2 && (
                  <button
                    className="mb-3 about-button"
                    onClick={() => {
                      const showItem = filter.length + 2;
                      setShow(showItem);
                    }}
                  >
                    Покажи повеќе
                  </button>
                )}
              </div>
            </div>
            <hr />
            <div>
              <h6>Повеќе од {cookName}</h6>
            </div>
          </div>
        </Modal.Body>
        <Modal.Footer className="modal-main">
          <Button variant="danger" onClick={() => initModal(false, "", 0)}>
            Затвори
          </Button>
          <Button variant="dark" onClick={() => initModal(false, "", 0)}>
            во кошничка
          </Button>
        </Modal.Footer>
      </Modal>
    </div>
  );
}
