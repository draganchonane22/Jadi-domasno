import React, { useState } from "react";
import "./DetailsHeader.scss";
import { IoHomeOutline } from "react-icons/io5";
import { IoLocationSharp } from "react-icons/io5";
import { MdOutlineFoodBank } from "react-icons/md";
import { MdReviews } from "react-icons/md";
import { AiFillStar } from "react-icons/ai";
import GetLocation from "../getLocation/GetLocation";

export interface CookTypesD {
  id: number;
  imageUrl: number;
  name: string;
  rating: number;
  cuisines: string[];
  location: string;
  details: {
    thumbnailImageUrl: number;
    description: string;
    delivery: string;
    numberOfDelivery: number;
    reviews: number;
    imageUrl: number;
    title: string;
    prise: number;
  };
  reviewsList: {
    id: number;
    author: string;
    comment: string;
    stars: number;
  }[];
}

type Props = {
  data: CookTypesD;
  resultStars: number
};

export default function DetailsHeader({ data, resultStars }: Props) {
  const [showMore, setShowMore] = useState(false);
  const [show, setShow] = useState(false);

  const handleChange = () => {
    setShow((prev) => !prev);
  };
 

  return (
    <div className="bg__color">
      <img
        className="about__img-hero"
        src={`/images/hrana${data.details.thumbnailImageUrl}.jpg`}
        alt="img"
      />
      <div className="row mx-auto py-5 justify-content-around">
        <div className="col-3">
          <img
            className="about__img-details"
            src={`/images/header${data.imageUrl}.jpg`}
            alt="img"
          />
        </div>
        <div className="col-9">
          <div className="d-flex justify-content-between">
            <p className="about__cook-name mb-4">{data.name}</p>
            <div className="mr-5"></div>
          </div>
          <div className="d-flex">
            <span className="font-big1 mr-4">
              Традиционални оброци <img src="/images/Layer3.png" alt="img" />
            </span>

            <span className="font-big1 mx-5">
              <IoHomeOutline />
              {data.location}
            </span>

            <span
              className="font-big1 ml-5 main__button-custom"
              onClick={handleChange}
            >
              <IoLocationSharp />
              <span className="h3">Види точна адреса </span>
              {show && <GetLocation show={show} />}
            </span>
          </div>

          <div className="mt-4">
            <span className="about-span">
              <AiFillStar className="about__icons" />
              {""}
              {resultStars.toFixed(1)}
            </span>
            <span className="about-span">
              <MdReviews className="about__icons" /> Прегледи{" "}
              {data.details.reviews}
            </span>
            <span className="about-span">
              <MdOutlineFoodBank className="about__icons" /> Испораки{" "}
              {data.details.numberOfDelivery}
            </span>
          </div>
          <p className="mt-4">
            {showMore
              ? data.details.description
              : `${data.details.description.substring(0, 200)}`}
            <span className="h5">
              <div
                className="about__dsc"
                onClick={() => setShowMore(!showMore)}
              >
                {showMore ? "Затвори" : "Дознај повеке!"}
              </div>
            </span>
          </p>
        </div>
      </div>
    </div>
  );
}
