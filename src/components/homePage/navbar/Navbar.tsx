import React from "react";
import "./Navbar.scss";
import { Link } from "react-router-dom";
import { IoHomeOutline } from "react-icons/io5";
import { RiUserHeartLine } from "react-icons/ri";
import { GiCook } from "react-icons/gi";
import { RiFileEditLine } from "react-icons/ri";
import { IoRestaurantOutline } from "react-icons/io5";
import { MdOutlineForum } from "react-icons/md";
import { CgProfile } from "react-icons/cg";
import { FaShopify } from "react-icons/fa";
import { useCookContext } from "../../../contex/CookContext";

export default function Navbar() {
  const { active, handleActive } = useCookContext();
  return (
    <div className="container-fluid p-0 Navbar">
      <nav className="navbar navbar-expand-lg navbar-light bg-light navShadow">
        <Link to={"/"} className="mr-4 text-center">
          <div
            className={active === "/" ? "active" : ""}
            onClick={() => handleActive("/")}
          >
            <IoHomeOutline className="about__icon" />
            <p>Јади Домашно</p>
          </div>
        </Link>
        <button
          className="navbar-toggler"
          type="button"
          data-toggle="collapse"
          data-target="#navbarSupportedContent"
          aria-controls="navbarSupportedContent"
          aria-expanded="false"
          aria-label="Toggle navigation"
        >
          <span className="navbar-toggler-icon"></span>
        </button>

        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav mr-auto">
            <Link to={"/how"} className="mr-4 text-center">
              <li>
                <div
                  className={active === "/how" ? "active" : ""}
                  onClick={() => handleActive("/how")}
                >
                  <RiUserHeartLine className="about__icon" />
                  <p className="">Како</p>
                </div>
              </li>
            </Link>
            <Link to={"/cooks"} className="mr-4">
              <li className="nav-item text-center">
                <div
                  className={active === "/cooks" ? "active" : ""}
                  onClick={() => handleActive("/cooks")}
                >
                  <GiCook className="about__icon" />
                  <p>Готвачи</p>
                </div>
              </li>
            </Link>
            <Link to={"offer"} className="mr-4">
              <li className="nav-item text-center">
                <div
                  className={active === "/offer" ? "active" : ""}
                  onClick={() => handleActive("/offer")}
                >
                  <RiFileEditLine className="about__icon" />
                  <p>Понуда</p>
                </div>
              </li>
            </Link>
            <Link to={"/menu"} className="mr-4">
              <li className="nav-item text-center">
                <div
                  className={active === "/menu" ? "active" : ""}
                  onClick={() => handleActive("/menu")}
                >
                  <IoRestaurantOutline className="about__icon" />
                  <p>Мени</p>
                </div>
              </li>
            </Link>
            <Link to={"forum"} className="mr-4">
              <li className="nav-item text-center">
                <div
                  className={active === "/forum" ? "active" : ""}
                  onClick={() => handleActive("/forum")}
                >
                  <MdOutlineForum className="about__icon" />
                  <p>Форум</p>
                </div>
              </li>
            </Link>
          </ul>
          <form className="form-inline my-2 my-lg-0">
            <input
              className="form-control mr-sm-2 rounded-pill shadow p-3 bg-white rounded"
              type="search"
              placeholder="&#xf0e0; Пребарај"
              aria-label="Search"
            />
            <ul className="navbar-nav mr-auto">
              <Link to={"/profil"} className="mr-4">
                <li className="nav-item text-center">
                  <div
                    className={active === "/profil" ? "active" : ""}
                    onClick={() => handleActive("/profil")}
                  >
                    <CgProfile className="about__icon" />
                    <p>Профил</p>
                  </div>
                </li>
              </Link>
              <Link to={"/buy"}>
                <li className="nav-item text-center">
                  <div
                    className={active === "/buy" ? "active" : ""}
                    onClick={() => handleActive("/buy")}
                  >
                    <FaShopify className="about__icon" />
                    <p>Купи</p>
                  </div>
                </li>
              </Link>
            </ul>
          </form>
        </div>
      </nav>
    </div>
  );
}
