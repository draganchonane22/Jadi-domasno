import React from "react";
import "./HomePageUsers.scss";
import UserCards from "../userCards/UserCards";
import { useCookContext } from "../../../contex/CookContext";

export default function HomePageUsers() {
  const { userData } = useCookContext();

  return (
    <div className="row w-castom">
      <div className="card-deck">
        {userData &&
          userData.map((user) => (
            <UserCards key={user.id} {...user}></UserCards>
          ))}
        ;
      </div>
    </div>
  );
}
