import React from "react";
import "./Footer.scss";
import FacebookRoundedIcon from "@mui/icons-material/FacebookRounded";
import TwitterIcon from "@mui/icons-material/Twitter";
import LinkedInIcon from "@mui/icons-material/LinkedIn";
import YouTubeIcon from "@mui/icons-material/YouTube";

export default function Footer() {
  return (
    <div className="container-fluid bg-color deko">
      <div className="row justify-content-between py-5 my-auto">
        <div className="text-color col">
          <div className="row">
            <div className="logo col my-auto">
              <img className="w-100" src="/images/logo.png" alt="img" />
            </div>
            <div className="mt-5 col my-auto">
              <h2 className="font-weight-light">Јади Домашно</h2>
              <p>Јадете здраво. Јадете подобро</p>
              <div className="mt-4">
                <a
                  href="https://www.facebook.com/"
                  target="_blank"
                  rel="noopener noreferrer"
                >
                  <FacebookRoundedIcon className="mr-2" />
                </a>
                <a
                  href="https://www.twitter.com/"
                  target="_blank"
                  rel="noopener noreferrer"
                >
                  <TwitterIcon className="mr-3" />
                </a>
                <a
                  href="https://www.linkedin.com/"
                  target="_blank"
                  rel="noopener noreferrer"
                >
                  <LinkedInIcon className="mr-3" />
                </a>
                <a
                  href="https://www.youtube.com/"
                  target="_blank"
                  rel="noopener noreferrer"
                >
                  <YouTubeIcon />
                </a>
              </div>
            </div>
          </div>
        </div>
        <div className="text-color d-flex justify-content-center text-left col my-auto">
          <ul>
            <li>
              <a href="/">За Нас</a>
            </li>
            <li>
              <a href="/">Мени</a>
            </li>
            <li>
              <a href="/">Стани Готвач</a>
            </li>
            <li>
              <a href="/">Најави се</a>
            </li>
            <li>
              <a href="/">FAQ</a>
            </li>
          </ul>
        </div>

        <div className="text-color col my-auto">
          <ul>
            <li>
              <a href="/">Правни</a>
            </li>
            <li>
              <a href="/">Политика за приватност</a>
            </li>
            <li>
              <a href="/">Услови за веб страница</a>
            </li>
            <li>
              <a href="/">Прифатлива политика за користење</a>
            </li>
            <li>
              <a href="/">Политика за колачиња</a>
            </li>
            <li>
              <a href="/">Општи услови</a>
            </li>
          </ul>
        </div>

        <div className="text-color col my-auto text-center mr-5">
          <ul>
            <li>
              <a href="/">Пратете ги новостите</a>
            </li>
            <li>
              <button className="rounded-pill px-5 py-2">
                Електонска пошта
              </button>
            </li>
            <li>
              <button className="rounded-pill px-5 py-2 bg-or text-white">
                Претплати се
              </button>
            </li>
          </ul>
        </div>
      </div>
    </div>
  );
}
