import React from "react";
import "./HomePageCook.scss";
import HomeCookCard from "../homeCookCard/HomeCookCard";
import { useCookContext } from "../../../contex/CookContext";
import { Link } from "react-router-dom";



export default function HomePageCook() {
  const { cooksData, handleActive } = useCookContext();

  return (
    <div className="container-fluid">
      <div className="row">
        <div className="col-4">
          <hr className="color mr-0 text-right" />
        </div>
        <div className="col-4">
          <p className="p-castom">Запознајте ги нашите готвачи</p>
        </div>
        <div className="col-4">
          <hr className="color ml-0 text-left" />
        </div>
      </div>
      <div className="row my-5">
        {cooksData &&
          cooksData.slice(0, 3).map((cook) => (
            <HomeCookCard key={cook.id} {...cook}></HomeCookCard>
          ))}
      </div>
      <div className="text-center py-5">
        <Link to={"/cooks"}>
        <button className="btn text-light"  onClick={() => handleActive("/cooks")}>Кон готвачи</button>
        </Link>
      </div>
    </div>
  );
}
