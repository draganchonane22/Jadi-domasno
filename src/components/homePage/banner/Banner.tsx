import React from "react";
import "./Banner.scss";

export default function Banner() {
  return (
    <div className="dek">
      <div className="py-2 wrapper-3 container-fluid">
        <div className="castom-position0">
          <img className="w-100" src="./images/img4.png" alt="img" />
        </div>
        <img className="castom-position1" src="./images/img4.png" alt="img" />
        <img className="castom-position2" src="./images/img5.png" alt="img" />

        <div className="d-flex justify-content-between">
          <div className="wrapper-1">
            <div className="d-flex align-items-center just justify-content-center">
              <div className="logo">
                <img className="w-100" src="/images/logo.png" alt="img" />
              </div>
              <div>
                <p className="pf-castom">Јади домашно</p>
              </div>
            </div>
            <div className="text-center">
              <p className="pd-castom mb-4">Вкусот на твоето соседство!</p>
              <div className="d-flex justify-content-between">
                <div className="b1">
                  <input
                    className="bg-input"
                    type="text"
                    placeholder="Внеси адреса"
                  />
                </div>
                <div className="b2">
                  <button className="bg-button text-light">
                    Погледни резултати
                  </button>
                </div>
              </div>
            </div>
          </div>
          <div className="wrapper-2">
            <div className="my-auto">
              <img
                className="homeImg w-100 text-center"
                src="/images/homeImg.png"
                alt="img"
              />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
