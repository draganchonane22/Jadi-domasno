import React from "react";
import "./HomeCookCard.scss";
import { Rating } from "@mui/material";
import { IoHomeOutline } from "react-icons/io5";
import { Link } from "react-router-dom";
import { CookTypesD } from "../../details/detailsHeader/DetailsHeader";


export default function HomeCookCard({
  id,
  imageUrl,
  name,
  rating,
  cuisines,
  location,
}: CookTypesD) {
  return (
    <div className="col-4 my-3">
      <div className="container1 d-flex justify-content-center">
        <div className="card1">
          <div className="position-relative">
            <img
              className="w-100 mb-4 about-img"
              src={`/images/header${imageUrl}.jpg`}
              alt="img"
            />
            {rating && rating >= 4 && rating <= 5 && (
              <span className="badge-position">
                <img src="/images/badge1.svg" alt="badge" />
              </span>
            )}
          </div>
          <div className="de">
            <div className="d-flex justify-content-between">
              <h5 className="ml-3">{name}</h5>
              <span>
                <Rating name="read-only" value={rating} readOnly />
              </span>
            </div>
            <div className="d-flex justify-content-between">
              <div>
                {cuisines?.map((item, i) => (
                  <p className="mb-2" key={i}>
                    {item}
                  </p>
                ))}
              </div>
              <div>
                <IoHomeOutline />
                <span className="mb-5 mx-2">{location}</span>
              </div>
            </div>
            <div className="text-right mr-2">
              <Link to={`/cookDetails/${id}`}>
                <button>Дознај повеќе...</button>
              </Link>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
