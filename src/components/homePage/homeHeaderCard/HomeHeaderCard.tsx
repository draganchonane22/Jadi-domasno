import "./HomeHeaderCard.scss";
import React from "react";

export interface HomeData {
  id: number;
  imageUrl: number;
  heading: string;
  title: string;
}



export default function HomeHeaderCard({ imageUrl, heading, title }: HomeData) {
  return (
    <>
      <div>
        <div className="mb-3 text-center">
          <img
            className="img-round img-scale about-img"
            src={`/images/header${imageUrl}.jpg`}
            alt="img"
          />
        </div>
        <p className="p-castom mb-3">{heading}</p>
        <p className="p1-castom ">{title}</p>
      </div>
    </>
  );
}
