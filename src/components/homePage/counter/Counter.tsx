import React from "react";
import "./Counter.scss";
import CountUp from "react-countup";



export default function Counter() {
  return (
    <div className="my-3 container-fluid dek">
      <img className="castom-position5" src="./images/img4.png" alt="img"/>
      <img className="castom-position6" src="./images/img4.png" alt="img"/>
      <hr className="hr-castom" />
      <div className="row my-5">
        <div className="col-4 text-center">
          <div className="sm-width mx-auto mb-4">
            <img src="/images/img1.png" alt="img"/>
          </div>
          <CountUp
            className="c"
            end={10900}
            enableScrollSpy
            suffix="+"
            separator=" "
          />
          <p className="mt-3 f-castom">задоволни клиенти</p>
        </div>
        <div className="col-4 text-center">
          <div className="sm-width mx-auto mb-3">
            <img src="/images/img2.png" alt="img" />
          </div>
          <CountUp
            className="c"
            end={13765}
            enableScrollSpy
            suffix="+"
            separator=" "
          />
          <p className="mt-3 f-castom">подготвени јадења</p>
        </div>
        <div className="col-4 text-center">
          <div className="sm-width mx-auto mb-3">
            <img className="w-100" src="/images/img3.png" alt="img"/>
          </div>
          <CountUp className="c" end={864} enableScrollSpy suffix="+"/>
          <p className="mt-3 f-castom">среќни готвачи</p>
        </div>
      </div>
      <hr className="hr-castom"/>
    </div>
  );
}
