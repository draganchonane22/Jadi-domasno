import React from "react";
import "./UserCards.scss";

export interface Users {
  id: number;
  imageUrl: number;
  date: string;
  rating: number,
  description: string;
  name: string;
  age: number;
}

export default function UserCards({ imageUrl, description, name, age }: Users) {
  return (
    <div className="col-4 mx-auto">
      <div className="card card__main">
        <img
          src={`/images/user${imageUrl}.png`}
          className="card-img-top my-5 de"
          alt="img"
        />
        <div className="card-body">
          <p className="card-text mb-4">{description}</p>
          <span>{name}</span>,{""}<span>{age} години</span>
        </div>
      </div>
    </div>
  );
}
