import React from "react";
import "./Header.scss";
import HomeHeaderCard  from "../homeHeaderCard/HomeHeaderCard";
import { useCookContext } from "../../../contex/CookContext";



export default function Header() {
  
  const { descData } = useCookContext();
  return (
    <div className="pad-120 container-fluid">
      <div className="row">
        <div className="col-4">
          <hr className="mr-0 text-right" />
        </div>
        <div className="col-4">
          <p className="p-castom mb-5">Нашите вредности</p>
        </div>
        <div className="col-4">
          <hr className="ml-0 text-left" />
        </div>
      </div>
      <div className="d-flex align-items-center justify-content-center justify-content-around">
        {descData &&
          descData.map((item) => (
            <HomeHeaderCard key={item.id} {...item}></HomeHeaderCard>
          ))}
      </div>
      <div className="text-center mt-5">
        <button className="btn text-light">Дознај повеќе за нас</button>
      </div>
    </div>
  );
}
