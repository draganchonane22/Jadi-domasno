import React from "react";
import "./HomeLogIn.scss";

export default function HomeLogIn() {
  return (
    <div className="dek">
      <img className="castom-position3" src="./images/img7.png" alt="img" />
      <img className="castom-position4" src="./images/img5.png" alt="img" />
      <div className="row wrapper1 mb-5">
        <div className="col-5 my-auto de">
          <hr />
        </div>
        <div className="col-2 text-center">
          <img src="/images/domat1.png" alt="img" />
        </div>
        <div className="col-5 my-auto de">
          <hr />
        </div>
      </div>
      <p className="f-main text-center">
        Стани дел од семејството <span>Јади домашно</span>
      </p>
      <div className="wrapper">
        <div className="row my-5">
          <div className="col text-center de-hr">
            <img src="/images/group1.png" alt="img" />
            <p className="mb-5">Сакам да нарачувам храна</p>
            <hr className="mb-5" />
            <div className="ml-5">
              <ul className="list-unstyled text-left de-li">
                <li className="mb-3">
                  <img src="/images/image41.png" alt="img" />
                  <a href="#">Регистрирај се на платформата како клиент!</a>
                </li>
                <li className="mb-3">
                  <span>
                    <img src="/images/image41.png" alt="img" />
                  </span>
                  <a href="#" className="text-center">
                    Одбери дали сакаш повеке подготвена храна.
                  </a>
                </li>
                <li className="mb-3">
                  <img src="/images/image42.png" alt="img" />
                  <a href="#">Нарачај брзо и лесно преку нашата платформа!</a>
                </li>
                <li className="mb-5">
                  <img src="/images/image43.png" alt="img" />
                  <a href="#">
                    Подигни ја нарачката и уживај во вкусот на домашната храна.
                  </a>
                </li>
              </ul>
            </div>
            <button className="btn">стани клиент</button>
          </div>
          <div className="col text-center de-hr">
            <img src="/images/group2.png" alt="img" />
            <p className="mb-5">Сакам да нарачувам храна</p>
            <hr className="mb-5" />
            <div className="ml-5">
              <ul className="list-unstyled text-left de-li">
                <li className="mb-3">
                  <img src="/images/image41.png" alt="img" />
                  <a href="#">Регистрирај се на платформата како готвач!</a>
                </li>
                <li className="mb-3">
                  <img src="/images/image41.png" alt="img" />
                  <a href="#">Пополни го целосно твојот профил!</a>
                </li>
                <li className="mb-3">
                  <img src="/images/image42.png" alt="img" />
                  <a href="#">Примај нарачки и готви вкусни јадења!</a>
                </li>
                <li className="mb-5">
                  <img src="/images/image43.png" alt="img" />
                  <a href="#">Испорачувај ги твоите нарачки и заработи.</a>
                </li>
              </ul>
            </div>
            <button className="btn">стани готвач</button>
          </div>
        </div>
      </div>
    </div>
  );
}
