import React from "react";
import "./SearchCooks.scss";
import Rating from "@mui/material/Rating";
import { GiClick } from "react-icons/gi";
import { useCookContext } from "../../contex/CookContext";

export interface Cuisines {
  id: number;
  nameBtn: string;
}



export default function SearchCooks() {
  const {
    handleCuisines,
    stateCuisines,
    cuisines,
    clearFilter,
    ratingCuisines,
    handleRatingCuisines,
  } = useCookContext();

  return (
    <div className="p-3">
      <div className="p-3 about-bg rounded">
        <p className="text-center my-3">Покажи по рејтинг</p>
        <form>
          <div className="row">
            <div className="col-7 my-auto">
              <Rating name="read-only" value={3} readOnly />
            </div>
            <div className="col-2 my-auto">3</div>
            <div className="col-3 my-auto">
              <div className="form-group mt-3">
                <div className="form-check">
                  <input
                    className="form-check-input"
                    type="radio"
                    name="search-type"
                    id="flexRadioDefault4"
                    value="3"
                    checked={ratingCuisines === "3"}
                    onChange={(e) => handleRatingCuisines(e.target.value)}
                  />
                  <label
                    className="form-check-label"
                    htmlFor="flexRadioDefault4"
                  >
                    <GiClick className="about__icons" />
                  </label>
                </div>
              </div>
            </div>
          </div>

          <div className="row">
            <div className="col-7 my-auto">
              <Rating name="read-only" value={4} readOnly />
            </div>
            <div className="col-2 my-auto">4+</div>
            <div className="col-3 my-auto">
              <div className="form-group mt-3">
                <div className="form-check">
                  <input
                    className="form-check-input"
                    type="radio"
                    name="search-type"
                    id="flexRadioDefault5"
                    value="4"
                    checked={ratingCuisines === "4"}
                    onChange={(e) => handleRatingCuisines(e.target.value)}
                  />
                  <label
                    className="form-check-label"
                    htmlFor="flexRadioDefault5"
                  >
                    <GiClick className="about__icons" />
                  </label>
                </div>
              </div>
            </div>
          </div>

          <div className="row mb-3">
            <div className="col-7 my-auto">
              <Rating name="read-only" value={5} readOnly />
            </div>
            <div className="col-2 my-auto">5</div>
            <div className="col-3 my-auto">
              <div className="form-group mt-3">
                <div className="form-check">
                  <input
                    className="form-check-input"
                    type="radio"
                    name="search-type"
                    id="flexRadioDefault6"
                    value="5"
                    checked={ratingCuisines === "5"}
                    onChange={(e) => handleRatingCuisines(e.target.value)}
                  />
                  <label
                    className="form-check-label"
                    htmlFor="flexRadioDefault6"
                  >
                    <GiClick className="about__icons" />
                  </label>
                </div>
              </div>
            </div>
          </div>
        </form>
        {ratingCuisines && (
          <button
            className="ml-5 mb-3 btn__shadow"
            onClick={() => clearFilter("ratingCuisines")}
          >
            Избриши филтер
          </button>
        )}
        <hr className="about-hr" />
        <p className="my-3 text-center">Алергени</p>
        <div className="d-flex flex-wrap mb-5">
          {cuisines.map((btn, i) => (
            <div key={i}>
              <button
                className={`about__button m-1 ${
                  stateCuisines === btn.nameBtn ? "activ" : undefined
                }`}
                key={btn.id}
                value={btn.nameBtn}
                onClick={() => handleCuisines(btn.nameBtn)}
              >
                {btn.nameBtn}
              </button>
            </div>
          ))}
          {stateCuisines && (
            <button
              className="btn__shadow my-3"
              onClick={() => clearFilter("cuisines")}
            >
              Избриши филтер
            </button>
          )}
        </div>
      </div>
    </div>
  );
}
