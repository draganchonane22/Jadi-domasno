import React from "react";
import "./MenuCardAll.scss";
import Rating from "@mui/material/Rating";
import { IoHomeOutline } from "react-icons/io5";
import { CiDeliveryTruck } from "react-icons/ci";
import { TbTruckOff } from "react-icons/tb";
import { useCookContext } from "../../../contex/CookContext";

export interface MenuData {
  id: number;
  cookId: number;
  imageUrl: number;
  cookImageUrl: number;
  cookName: string;
  foodName: string;
  price: number | number[];
  rating: number;
  location: number;
  delivery: string;
  category: string;
  available: string;
  alergen: string;
  time: string;
  availablePortion: number;
  description: string;
  ingredients: string;
  nutrition: string;
  alergies: string;
}

export default function MenuCardAll({
  cookId,
  imageUrl,
  cookImageUrl,
  foodName,
  price,
  rating,
  location,
  delivery,
}: MenuData) {
  const { initModal } = useCookContext();
  return (
    <div className="col-4">
      <div className="card card-main about-bg-card">
        <div>
          <img
            src={`./images/hrana${imageUrl}.jpg`}
            className="card-img-top w-100 card-body-main"
            alt="img"
          />
        </div>
        {delivery === "yes" ? (
          <CiDeliveryTruck className="about__icons about-position1" />
        ) : (
          <TbTruckOff className="about__icons about-position1" />
        )}
        <div className="card-body card-body row">
          <div className="col-4 px-0">
            <img
              src={`./images/user${cookImageUrl}.png`}
              className="img-main"
              alt="img"
            />
          </div>
          <div className="col-5">
            <h6 className="card-title">{foodName}</h6>
            <Rating name="read-only" value={rating} readOnly className="" />
          </div>
          <div className="col-3">
            <span className="about-color">{price} ден</span>
          </div>
          <div>
            <button
              className="btn__shadow"
              onClick={() => initModal(true, foodName, cookId)}
            >
              Vo kosnicka
            </button>
          </div>
          <span className="ml-5 about-poz">
            <IoHomeOutline className="about__icon" /> {location}m
          </span>
        </div>
      </div>
    </div>
  );
}
