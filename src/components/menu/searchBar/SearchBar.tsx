import React from "react";
import "./SearchBar.scss";
import Slider from "@mui/material/Slider";
import { useCookContext } from "../../../contex/CookContext";
import Rating from "@mui/material/Rating";
import { CiDeliveryTruck } from "react-icons/ci";
import { TbTruckOff } from "react-icons/tb";
import { GiClick } from "react-icons/gi";

const marks = [
  {
    value: 0,
    label: "0den",
  },
  {
    value: 1000,
    label: "1000den",
  },
];

export interface Alergeni {
  id: number;
  nameBtn: string;
}

export default function SearchBar() {
  const {
    alergeni,
    handleButton,
    clearFilter,
    stateAlergeni,
    availableFood,
    handleChange,
    handleAvailableFood,
    ratingFilter,
    handleRatingFilter,
    deliveryFilter,
    handleDeliveryFilter,
  } = useCookContext();

  return (
    <div className="p-3">
      <div className="p-3 about-bg">
        <form className="mt-3">
          <div className="form-group mt-3 ml-5">
            <div className="form-check">
              <input
                className="form-check-input"
                type="radio"
                name="search-type"
                id="flexRadioDefault1"
                value="Достапно веднаш"
                checked={availableFood === "Достапно веднаш"}
                onChange={(e) => handleAvailableFood(e.target.value)}
              />
              <label className="form-check-label" htmlFor="flexRadioDefault1">
                Достапно веднаш
              </label>
            </div>
            <div className="form-check">
              <input
                className="form-check-input"
                type="radio"
                name="search-type"
                id="flexRadioDefault2"
                value="Достапно утре"
                checked={availableFood === "Достапно утре"}
                onChange={(e) => handleAvailableFood(e.target.value)}
              />
              <label className="form-check-label" htmlFor="flexRadioDefault2">
                Достапно утре
              </label>
            </div>
            <div className="form-check">
              <input
                className="form-check-input"
                type="radio"
                name="search-type"
                id="flexRadioDefault3"
                value="Достапно по порачка"
                checked={availableFood === "Достапно по порачка"}
                onChange={(e) => handleAvailableFood(e.target.value)}
              />
              <label className="form-check-label" htmlFor="flexRadioDefault3">
                Достапно по порачка
              </label>
            </div>
          </div>
        </form>
        {availableFood && (
          <button
            className="ml-5 mb-3 btn__shadow"
            onClick={() => clearFilter("available")}
          >
            Избриши филтер
          </button>
        )}
        <hr className="about-hr" />
        <p className="py-3 text-center">Филтрирај по цена</p>
        <div className="about-slider">
          <Slider
            min={0}
            step={10}
            max={1000}
            marks={marks}
            defaultValue={0}
            onChange={handleChange}
            valueLabelDisplay="auto"
            aria-labelledby="non-linear-slider"
          />
        </div>
        <hr className="about-hr" />
        <p className="my-3 text-center">Алергени</p>
        <div className="d-flex flex-wrap mb-5">
          {alergeni.map((btn, i) => (
            <div key={i}>
              <button
                className={`about__button mb-3 mr-2 ${
                  stateAlergeni === btn.nameBtn ? "activ" : undefined
                }`}
                key={btn.id}
                value={btn.nameBtn}
                onClick={() => handleButton(btn.nameBtn)}
              >
                {btn.nameBtn}
              </button>
            </div>
          ))}
          {stateAlergeni && (
            <button
              className="btn__shadow"
              onClick={() => clearFilter("alergeni")}
            >
              Избриши филтер
            </button>
          )}
        </div>
        <hr className="about-hr" />
        <p className="text-center my-3">Покажи по рејтинг</p>
        <form>
          <div className="row">
            <div className="col-7 my-auto">
              <Rating name="read-only" value={3} readOnly />
            </div>
            <div className="col-2 my-auto">3</div>
            <div className="col-3 my-auto">
              <div className="form-group mt-3">
                <div className="form-check">
                  <input
                    className="form-check-input"
                    type="radio"
                    name="search-type"
                    id="flexRadioDefault4"
                    value="3"
                    checked={ratingFilter === "3"}
                    onChange={(e) => handleRatingFilter(e.target.value)}
                  />
                  <label
                    className="form-check-label"
                    htmlFor="flexRadioDefault4"
                  >
                    <GiClick className="about__icons" />
                  </label>
                </div>
              </div>
            </div>
          </div>
          <div className="row">
            <div className="col-7 my-auto">
              <Rating name="read-only" value={4} readOnly />
            </div>
            <div className="col-2 my-auto">4+</div>
            <div className="col-3 my-auto">
              <div className="form-group mt-3">
                <div className="form-check">
                  <input
                    className="form-check-input"
                    type="radio"
                    name="search-type"
                    id="flexRadioDefault5"
                    value="4"
                    checked={ratingFilter === "4"}
                    onChange={(e) => handleRatingFilter(e.target.value)}
                  />
                  <label
                    className="form-check-label"
                    htmlFor="flexRadioDefault5"
                  >
                    <GiClick className="about__icons" />
                  </label>
                </div>
              </div>
            </div>
          </div>
          <div className="row mb-3">
            <div className="col-7 my-auto">
              <Rating name="read-only" value={5} readOnly />
            </div>
            <div className="col-2 my-auto">5</div>
            <div className="col-3 my-auto">
              <div className="form-group mt-3">
                <div className="form-check">
                  <input
                    className="form-check-input"
                    type="radio"
                    name="search-type"
                    id="flexRadioDefault6"
                    value="5"
                    checked={ratingFilter === "5"}
                    onChange={(e) => handleRatingFilter(e.target.value)}
                  />
                  <label
                    className="form-check-label"
                    htmlFor="flexRadioDefault6"
                  >
                    <GiClick className="about__icons" />
                  </label>
                </div>
              </div>
            </div>
          </div>
        </form>
        {ratingFilter && (
          <button
            className="ml-5 mb-3 btn__shadow"
            onClick={() => clearFilter("rating")}
          >
            Избриши филтер
          </button>
        )}
        <hr className="about-hr" />
        <form className="">
          <div className="row mt-3">
            <div className="col-7 my-auto">
              <p>Достава</p>
            </div>
            <div className="col-2">
              <CiDeliveryTruck className="about__icons about-position1 my-auto" />
            </div>
            <div className="col-3 my-auto">
              <div className="form-group mt-3">
                <div className="form-check">
                  <input
                    className="form-check-input"
                    type="radio"
                    name="search-type"
                    id="flexRadioDefault7"
                    value="yes"
                    checked={deliveryFilter === "yes"}
                    onChange={(e) => handleDeliveryFilter(e.target.value)}
                  />
                  <label
                    className="form-check-label"
                    htmlFor="flexRadioDefault7"
                  >
                    <GiClick className="about__icons" />
                  </label>
                </div>
              </div>
            </div>
          </div>

          <div className="row">
            <div className="col-7 my-auto">
              <p>Подигање</p>
            </div>
            <div className="col-2">
              <TbTruckOff className="about__icons about-position1 my-auto" />
            </div>
            <div className="col-3 my-auto">
              <div className="form-group mt-3">
                <div className="form-check">
                  <input
                    className="form-check-input"
                    type="radio"
                    name="search-type"
                    id="flexRadioDefault8"
                    value="no"
                    checked={deliveryFilter === "no"}
                    onChange={(e) => handleDeliveryFilter(e.target.value)}
                  />
                  <label
                    className="form-check-label"
                    htmlFor="flexRadioDefault8"
                  >
                    <GiClick className="about__icons" />
                  </label>
                </div>
              </div>
            </div>
            {deliveryFilter && (
              <button
                className="ml-5 mb-3 btn__shadow"
                onClick={() => clearFilter("delivery")}
              >
                Избриши филтер
              </button>
            )}
          </div>
        </form>
      </div>
    </div>
  );
}
