import React, { useState } from "react";
import "./Calendar.scss";
import {
  Datepicker,
  DatepickerEvent,
} from "@meinefinsternis/react-horizontal-date-picker";
import { enUS } from "date-fns/locale";

export const Calendar = () => {
  const [date, setDate] = useState<{
    endValue: Date | null;
    startValue: Date | null;
    rangeDates: Date[] | null;
  }>({
    startValue: null,
    endValue: null,
    rangeDates: [],
  });
  const handleChange = (d: DatepickerEvent) => {
    const [startValue, endValue, rangeDates] = d;
    console.log(endValue);
    setDate((prev) => ({ ...prev, endValue, startValue, rangeDates }));
  };

  return (
    <div>
      <div className="wrapper2">
        <Datepicker
          onChange={handleChange}
          locale={enUS}
          startValue={date.startValue}
          endValue={date.endValue}
        />
      </div>
    </div>
  );
};
export default Calendar;
