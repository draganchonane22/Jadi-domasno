import React from "react";

import Banner from "../components/homePage/banner/Banner";
import Header from "../components/homePage/header/Header";
import HomePageCook from "../components/homePage/homePageCook/HomePageCook";
import Counter from "../components/homePage/counter/Counter";
import HomePageUsers from "../components/homePage/homePageUsers/HomePageUsers";
import HomeLogIn from "../components/homePage/homeLogIn/HomeLogIn";



export default function HomePage() {
  return (
    <div>
      <Banner />
      <Header />
      <HomePageCook />
      <Counter />
      <HomePageUsers />
      <HomeLogIn />
    </div>
  );
}
