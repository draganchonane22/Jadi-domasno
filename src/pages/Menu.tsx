import React from "react";
import Calendar from "../components/menu/calendar/Calendar";
import SearchBar from "../components/menu/searchBar/SearchBar";
import MenuCardAll from "../components/menu/menuCards/MenuCardAll";
import { useCookContext } from "../contex/CookContext";
import SinglePopUp from "../components/details/singlePopUp/SinglePopUp";

export default function Menu() {
  const {
    mondayData,
    stateAlergeni,
    value,
    availableFood,
    ratingFilter,
    deliveryFilter,
    showItems,
    show,
    foodName,
    cookId,
    cooksData,
  } = useCookContext();

  const cookPopUp = mondayData.find(
    (item) => item.cookId === cookId && item.foodName === foodName
  );

  const cookDetailse = cooksData.find(
    (item) => item.name === cookPopUp?.cookName
  );

  const starsSum = cookDetailse?.reviewsList
    .map((review) => review.stars)
    .reduce((a, b) => a + b);
  const resultStars =
    starsSum && cookDetailse && starsSum / cookDetailse.details.reviews;

  const filtered = mondayData.filter(
    (item) =>
      (stateAlergeni ? item.alergen === stateAlergeni : true) &&
      (value ? item.price <= value : true) &&
      (availableFood ? item.available === availableFood : true) &&
      (ratingFilter ? item.rating.toString() === ratingFilter : true) &&
      (deliveryFilter ? item.delivery === deliveryFilter : true)
  );
  const filter = filtered.slice(0, show);

  return (
    <div className="container-fluid">
      <div className="row wrapper1 mt-5 justify-content-center">
        <div className="col-2 my-auto de">
          <hr />
        </div>
        <div className="col-2 text-center">
          <p className="font-big">Мени</p>
        </div>
        <div className="col-2 my-auto de">
          <hr />
        </div>
      </div>
      <Calendar />
      <div className="">
        <div className="row">
          <div className="col-3">
            <SearchBar />
          </div>
          <div className="col-9 text-center">
            <div className="row about-section">
              {filter.map((item) => (
                <MenuCardAll key={item.id} {...item}></MenuCardAll>
              ))}
              {filter.length < filtered.length && filtered.length > 6 && (
                <button className="mb-5" onClick={() => showItems(filter)}>
                  Покажи повеќе
                </button>
              )}
            </div>
          </div>
        </div>
      </div>
      {cookPopUp && resultStars && (
        <SinglePopUp cookPopUp={cookPopUp} resultStars={resultStars} />
      )}
    </div>
  );
}
