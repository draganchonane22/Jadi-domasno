import React from "react";
import HowLogo from "../components/how/howLogo/HowLogo";
import HowCooks from "../components/how/howCooks/HowCooks";
import HowGurman from "../components/how/howGurman/HowGurman";
import { useCookContext } from "../contex/CookContext";

export default function How() {
  const { howState } = useCookContext();

  return (
    <div className="container-fluid">
      <HowLogo />
      {howState === "cook" ? <HowCooks /> : <HowGurman />}
    </div>
  );
}
