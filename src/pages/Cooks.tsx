import React from "react";
import SearchCooks from "../components/cooks/SearchCooks";
import { useCookContext } from "../contex/CookContext";
import CookCardAll from "../components/cooks/cooksCardAll/CookCardAll";

export default function Cooks() {
  const { cooksData, stateCuisines, ratingCuisines, show, showItemss } =
    useCookContext();
  const filtered = cooksData.filter(
    (item) =>
      (stateCuisines
        ? item.cuisines.find((item) => item === stateCuisines)
        : true) &&
      (ratingCuisines ? item.rating.toString() === ratingCuisines : true)
  );
  const filter = filtered.slice(0, show);

  return (
    <div className="container-fluid">
      <div className="row wrapper1 my-3 justify-content-center">
        <div className="col-2 my-auto de">
          <hr />
        </div>
        <div className="col-4 text-center">
          <p className="font-big">Нашите Готвачи</p>
        </div>
        <div className="col-2 my-auto de">
          <hr />
        </div>
      </div>
      <p className="text-center my-4">
        Јади Домашно поврзува талентирани готвачи со локални клиенти.
      </p>
      <p className="text-center mb-5 w-50 mx-auto">
        Lorem ipsum, dolor sit amet consectetur adipisicing elit. Quisquam
        cupiditate repellat, ducimus porro asperiores velit illum eaque vitae
        qui eligendi a dignissimos, alias architecto assumenda enim soluta,
        veniam placeat tempora. Lorem ipsum, dolor sit amet consectetur
        adipisicing elit. Voluptatem molestias aut veniam distinctio laudantium
        labore pariatur eius sit neque soluta illum ad, assumenda quos aliquam
        porro consequuntur facilis fuga corporis!
      </p>
      <div className="">
        <div className="row">
          <div className="col-3">
            <SearchCooks />
          </div>
          <div className="col-9 text-center">
            <div className="row about-section">
              {filter.map((item) => (
                <CookCardAll key={item.id} {...item}></CookCardAll>
              ))}
              {filter.length < filtered.length && filtered.length > 6 && (
                <button className="mb-5" onClick={() => showItemss(filter)}>
                  Покажи повеќе
                </button>
              )}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
