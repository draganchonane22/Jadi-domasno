import React from "react";
import { Navigate, useParams } from "react-router-dom";
import DetailsHeader, {
  CookTypesD,
} from "../components/details/detailsHeader/DetailsHeader";
import { useCookContext } from "../contex/CookContext";
import FoodCard from "../components/details/foodCard/FoodCard";
import FeedBack from "../components/details/feedback/FeedBack";
import Calendar from "../components/menu/calendar/Calendar";
import SinglePopUp from "../components/details/singlePopUp/SinglePopUp";

export default function CookDetails() {
  const { id } = useParams();
  let cookDetailse: CookTypesD | undefined;

  const { feedBackData, mondayData, cooksData, foodName } = useCookContext();

  if (id) {
    cookDetailse = cooksData.find((item) => item.id.toString() === id);
  }
  if (!cookDetailse) {
    return <Navigate to="/404" />;
  }
  const cookMeal = mondayData.filter(
    (item) => item.cookId.toString() === id && item.category === "Оброк"
  );
  const cookPopUp = mondayData.find(
    (item) => item.cookId.toString() === id && item.foodName === foodName
  );
  const cookSupplements = mondayData.filter(
    (item) => item.cookId.toString() === id && item.category === "Салати"
  );
  const starsSum = cookDetailse.reviewsList
    .map((review) => review.stars)
    .reduce((a, b) => a + b);
  const resultStars = starsSum / cookDetailse.details.reviews;
  return (
    <div>
      {cookDetailse && (
        <DetailsHeader data={cookDetailse} resultStars={resultStars} />
      )}
      <div>
        <div className="text-center">
          <p className="font-weight-bolder mt-2">Одбери датум:</p>
          <Calendar />
        </div>
        <h5 className="text-center my-5">ДОСТАПНИ ЈАДЕЊА ЗА НЕДЕЛА</h5>
        <div className="w-75 mx-auto">
          <div className="row">
            {cookMeal &&
              cookMeal.map((item) => (
                <FoodCard key={item.id} {...item}></FoodCard>
              ))}
          </div>
        </div>
        <h5 className="text-center my-5">
          ПРЕДЛОГ ДОДАТОЦИ КОН ЈАДЕЊА ЗА НЕДЕЛА
        </h5>
        <div className="w-75 mx-auto">
          <div className="row">
            {cookSupplements &&
              cookSupplements.map((item) => (
                <FoodCard key={item.id} {...item}></FoodCard>
              ))}
          </div>
        </div>
        <h5 className="text-center my-5">
          ПРЕПОРАКИ ЗА ЈАДЕЊАТА НА ГОТВАЧОТ ОД ПРЕТХОДНИ КОРИСНИЦИ
        </h5>
        <div className="w-75 mx-auto">
          <div className="row">
            {feedBackData &&
              feedBackData.map((item) => (
                <FeedBack key={item.id} item={item} />
              ))}
          </div>
        </div>
      </div>
      <div>
        {cookPopUp && (
          <SinglePopUp cookPopUp={cookPopUp} resultStars={resultStars} />
        )}
      </div>
    </div>
  );
}
