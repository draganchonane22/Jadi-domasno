import { useEffect, useState } from "react";

export type FetchStatusType = "loading" | "done";
export const useFetch = <T>(url: string) => {
  type FetchStateType = {
    data: T[];
    state: FetchStatusType;
  };
  const [fetchState, setFetchState] = useState<FetchStateType>({
    state: "loading",
    data: [],
  });

  useEffect(() => {
    fetch(url)
      .then((res) => res.json())
      .then((data) => {
        setFetchState({
          data,
          state: "done",
        });
      });
  }, [url]);

  return fetchState;
};
