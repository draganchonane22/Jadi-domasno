export interface Details {
  thumbnailImageUrl: number;
  description: string;
  delivery: boolean;
  numberOfDelivery: number;
  reviews: number;
  imageUrl: number;
  title: string;
  prise: number;
}

export interface MondayMeal {
  id: number;
  name: string;
  imageUrl: number;
  price: number;
  rating: number;
  description: string;
  time: string;
  delivery: boolean;
}

export interface MondaySupplement {
  id: number;
  name: string;
  imageUrl: number;
  price: number;
  rating: number;
  description: string;
  time: string;
  delivery: boolean;
}

export interface MondayDessert {
  id: number;
  name: string;
  imageUrl: number;
  price: number;
  rating: number;
  description: string;
  time: string;
  delivery: boolean;
}

export interface HomePageCook {
  id: number;
  imageUrl: number;
  name: string;
  rating: number;
  cuisines: string[];
  location: string;
  details: Details;
  mondayMeal: MondayMeal[];
  mondaySupplements: MondaySupplement[];
  mondayDesserts: MondayDessert[];
}

export interface HomePageDescription {
  id: number;
  imageUrl: number;
  heading: string;
  title: string;
}

export interface HomePageUser {
  id: number;
  imageUrl: number;
  description: string;
  name: string;
  age: number;
}

export interface CookTypes {
  homePageCook: HomePageCook[];
  homePageDescription: HomePageDescription[];
  homePageUsers: HomePageUser[];
}



  // mondayMeal: {
  //   id: number;
  //   name: string;
  //   imageUrl: number;
  //   price: number;
  //   rating: number;
  //   description: string;
  //   time: string;
  //   delivery: boolean;
  // };

  // mondaySupplements: {
  //   id: number;
  //   name: string;
  //   imageUrl: number;
  //   price: number;
  //   rating: number;
  //   description: string;
  //   time: string;
  //   delivery: boolean;
  // };

  // mondayDesserts: {
  //   id: number;
  //   name: string;
  //   imageUrl: number;
  //   price: number;
  //   rating: number;
  //   description: string;
  //   time: string;
  //   delivery: boolean;
  // };

